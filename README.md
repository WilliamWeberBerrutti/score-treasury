# Score Treasury

**Compatibility:** Classic Doom source ports supporting DECORATE.

These are the jewels from Tyrian, which give points, according to their type, which are as follows:

  - SmallMineral: +20 points
  - BigMineral: +50 points

In the file it's also coded a Treasury Spawner which spawns various jewels, just to show off their beauty! You can use it when destroying or opening an object, like a chest, for instance. Its called "EclipseMineralSpawner".
